#! /usr/bin/env python

# D - download
# E - extract
# R - read
# P - parse

import urllib
import zipfile
import re
import json
from Agent import Agent

filename 	= "trecfile"
filename_zip 	= filename+".zip"
filename_txt 	= filename+".txt"

num_agents_total = 0
num_agents_active = 0

def download():
	url = "https://www.trec.texas.gov/sites/default/files/high-value-data-sets/trecfile.zip"
	urllib.urlretrieve(url, filename_zip)

def extract():
	with zipfile.ZipFile(filename_zip, "r") as z:
		z.extractall("./")

def parseAllLines():
	with open(filename_txt) as infile:
	    for line in infile:
		parse(unicode(line, errors='ignore')) # just drops any bad bytes (non-utf8)

def parse(line):

	lineArray = re.split(r'\t+', line.rstrip('\r\n'))

	agent = Agent()
	agent.full_name 			= lineArray[2]
	agent.phone_number 			= lineArray[9]
	agent.email_address 			= lineArray[10]
	agent.license.type 			= lineArray[0]
	agent.license.number 			= lineArray[1]
	agent.license.status 			= lineArray[3]
	agent.license.original_date 		= lineArray[4]
	agent.license.expiration_date 		= lineArray[5]
	agent.mailing_address.line_1 		= lineArray[11]
	agent.mailing_address.line_2 		= lineArray[12]
	agent.mailing_address.line_3 		= lineArray[13]
	agent.mailing_address.city 		= lineArray[14]
	agent.mailing_address.state_code 	= lineArray[15]
	agent.mailing_address.zip_code 		= lineArray[16]
	agent.mailing_address.county_code 	= lineArray[17]

	print json.dumps(agent.__dict__, default=lambda o: o.__dict__)

	global num_agents_total
	num_agents_total +=1

	if agent.license.status == '20':
		global num_agents_active
		num_agents_active += 1	

if(__name__ == "__main__"):

	download()
	extract()
	parseAllLines()

	print "[+] Total records parsed: " + str(num_agents_total)
	print "[+] Num Active Licenses: " + str(num_agents_active)
