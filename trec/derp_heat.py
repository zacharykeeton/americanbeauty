#! /usr/bin/env python

# D - download
# E - extract
# R - read
# P - parse

import urllib
import zipfile
import re
import json

filename 	= "trecfile"
filename_zip 	= filename+".zip"
filename_txt 	= filename+".txt"

num_agents_total = 0
num_agents_active = 0
bad_indices = 0
county_count = {}

def download():
	url = "https://www.trec.texas.gov/sites/default/files/high-value-data-sets/trecfile.zip"
	urllib.urlretrieve(url, filename_zip)

def extract():
	with zipfile.ZipFile(filename_zip, "r") as z:
		z.extractall("./")

def parsealllines():
	with open(filename_txt) as infile:
	    for line in infile:
		parse(unicode(line, errors='replace')) # just drops any bad bytes (non-utf8)

def parse(line):

	global num_agents_total
	num_agents_total +=1

	lineArray = re.split(r'\t+', line.rstrip('\r\n'))
	
	county_code = lineArray[17]
	license_status = lineArray[3]

	if not license_status.isdigit(): # there is a name suffix and everything is shifted right by one
		license_status = lineArray[4]
		county_code = lineArray[18]

	if license_status != "20":
		# we only care about active status
		return
		
	if '@' not in lineArray[10]: # there is no email address and things are shifted left by one
		county_code = lineArray[16]

	global num_agents_active
	num_agents_active += 1	

	index = "48" + "0"*(3 - len(county_code)) + county_code

	if len(index) == 5 and index.isdigit():	
		global county_count
		try:
			county_count[index] += 1
		except:
			county_count[index] = 1
	else:
		global bad_indices
		bad_indices += 1

def writetocsv():

	import csv

	with open('mycsvfile.csv', 'w') as f:  # Just use 'w' mode in 3.x
	    w = csv.writer(f)
	    w.writerows(county_count.items())

if(__name__ == "__main__"):

	#download()
	#extract()
	parsealllines()
	writetocsv()

	print json.dumps(county_count)
	print "[+] Total records parsed: " + str(num_agents_total)
	print "[+] Num Active Licenses: " + str(num_agents_active)
	print "[-] Bad indices: " + str(bad_indices)
