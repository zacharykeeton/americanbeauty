from License import License
from Address import Address

class Agent:
        def __init__(self):
                self.full_name          = None
                self.phone_number       = None
                self.license            = License()
                self.mailing_address    = Address()
