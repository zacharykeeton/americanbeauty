### color_map.py
 
import csv
from BeautifulSoup import BeautifulSoup
 
# Read in unemployment rates
unemployment = {}
min_value = 100; max_value = 0
reader = csv.reader(open('mycsvfile.csv'), delimiter=",")
for row in reader:
    try:
        fips = str(row[0])
        rate = int(row[1])
        unemployment[fips] = rate
    except:
        pass
 
 
# Load the SVG map
svg = open('USA_Counties_with_FIPS_and_names.svg', 'r').read()
 
# Load into Beautiful Soup
soup = BeautifulSoup(svg, selfClosingTags=['defs','sodipodi:namedview'])
 
# Find counties
paths = soup.findAll('path')
 
# Map colors
colors = ["#F1EEF6", "#D4B9DA", "#C994C7", "#DF65B0", "#DD1C77", "#980043"]
 
# County style
path_style = 'font-size:12px;fill-rule:nonzero;stroke:#FFFFFF;stroke-opacity:1;stroke-width:0.1;stroke-miterlimit:4;stroke-dasharray:none;stroke-linecap:butt;marker-start:none;stroke-linejoin:bevel;fill:'
 
# Color the counties based on unemployment rate
for p in paths:
     
    if p['id'] not in ["State_Lines", "separator"]:
        try:
            rate = unemployment[p['id']]
        except:
            continue
             
         
        if rate > 21828:
            color_class = 5
        elif rate > 16371:
            color_class = 4
        elif rate > 10914:
            color_class = 3
        elif rate > 5458:
            color_class = 2
        elif rate > 1:
            color_class = 1
        else:
            color_class = 0
 
        color = colors[color_class]
        p['style'] = path_style + color
 
print soup.prettify()
